# Customized Debian Container

## Description
A customized "fat" Debian Docker image with some tools pre-installed for testing and development.

This image is built on top of the [Debian](https://www.debian.org/) and includes several tools that may be useful for testing, development or sysadmin tasks. This is by no means a small container, and it quite likely contains unnecessary attack surface for application specific containers. As such I would strongly advice against using this container as a base image for production environment or applications. However this might be useful for developing and debugging before the production.

Personally I am using it a lot to separate applications from each other when testing things so that different instances don't mess up with each other while they store authentication, cache or similar data to the home directory. 

## Installing
No installation is required for basic usage. Just pull the container and run it.

## Running
The container can be run as any interactive container. It starts with bash shell as root user.

For example:
```
docker 'run' \
        --rm \
        --interactive \
        --tty \
        'quay.io/mireiawen/debian:latest'
```

Running the container as regular user requires customized image that contains the correct user. Build can be done with a custom Dockerfile and regular Docker build commands. There is also helper script for building that is better documented in the Building -section. To run the container as a regular user, just run the created container, or use the provided helper script that is documented below.

### Container starting helper
The `start-container` -helper starts the container in interactive mode and removes it after quitting. The script tries to mount the Docker socket to the container for Docker in Docker -use. 
**Note:** GNU getopt is required to run the helper script.

| Short option     | Long option             | Environment variable     | Default                    | Description                                                                                    |
|------------------|-------------------------|--------------------------|----------------------------|------------------------------------------------------------------------------------------------|
| `-m`             | `--mount-home`          | `MOUNT_TYPE="rw"`        | No mount                   | Mount the current user home directory to the container and run the user container              |
| `-r`             | `--mount-home-readonly` | `MOUNT_TYPE="ro"`        | No mount                   | Mount the current user home directory to the container as read-only and run the user container |
| `-u`             | `--user-tag`            | `CONTAINER_TAG="<value>` | "$(whoami)"                | Run the user container                                                                         |
| `-i <image>`     | `--image <image>`       | `IMAGE_SOURCE=<value>`   | "quay.io/mireiawen/debian" | Change the default image used                                                                  |
| `-t <tag>`       | `--tag <tag>`           | `CONTAINER_TAG=<value>`  | "$(whoami)"                | Run the container with specific tag                                                            |
| `-e <key=value>` | `--env <key=value>`     | (none)                   | None                       | Add environment variables, like in Docker command. Can be specified multiple times             |
| `-v <volume>`    | `--volume <volume>`     | (none)                   | None                       | Add volume, like in Docker `--volume` parameter. Script tries to parse the mount point and chown it recursively to the user when running with mount-home or mount-home-readonly. Can be specified multiple times |

## Building
There is an automated build available at Quay.io repository with name `quay.io/mireiawen/debian`.

The build script has originally been mostly for the user image building, and that is why the main image is referred as a source image. The script will pull the main image if one exists, then re-build it with current `Dockerfile` and after that builds the user-mode image based on `Dockerfile.user.j2` file.
**Note:** GNU getopt is required to run the build script.

| Short option     | Long option             | Environment variable     | Default                    | Description                                                                                    |
|------------------|-------------------------|--------------------------|----------------------------|------------------------------------------------------------------------------------------------|
| `-c`             | `--use-container`       | `USE_CONTAINER="1"`      | Not using container        | Use the container itself to help building the container, to avoid installing extra tools       |
| `-u`             | `--username <username>` | `USERNAME=<value>`       | "$(whoami)"                | Set the user's usedname to be used inside the container                                        |
| `-n`             | `--name <name>`         | `NAME=<value>`           | `getent` to get for $USER  | Set the user's real name to be used inside the container                                       |
| `-t`             | `--user-tag <tag>`      | `USER_TAG=<value>`       | "$(whoami)"                | Set the tag to be used for the user container                                                  |
| `-l`             | `--locales`             | `LOCALES=<value>`        | "en_US.UTF-8 UTF-8"        | Set the locales to be generated inside the user container, comma separated list                |
| `-i`             | `--source-image`        | `IMAGE_SOURCE=<value>`   | "quay.io/mireiawen/debian" | The image name to use, without the tags                                                        |
| `-s`             | `--source-tag`          | `IMAGE_TAG=<value>`      | "latest"                   | The root-user image to build                                                                   |
|                  | `--container-name`      | `CONTAINER_NAME=<value>` | "Debian $USERNAME"         | The name for the container to be set in the user container shell prompt                        |
|                  | `--uid`                 | `CONTAINER_UID=<value>`  | "$UID"                     | The UID for the user inside the user container                                                 |

## Available external tools
* Some common build toolchain utilities
* [Debian Backports](https://backports.debian.org/) are enabled
* [Package installer script](https://github.com/bitnami/minideb) `install_packages` from `docker.io/bitnami/minideb`
* [Python 3](https://www.python.org/) package installer `pip3` from Debian repository
* [gosu](https://github.com/tianon/gosu) `gosu` command
* [MariaDB client](https://mariadb.org/) `mysql` from  the Debian repository
* [Docker client](https://www.docker.com/) `docker` as Docker in Docker (DinD) from `docker.io/docker:dind`
* [Ansible CLI](https://www.ansible.com/) `ansible` from Python Package Index
* [Ansible linter](https://github.com/ansible/ansible-lint) `ansible-lint` from Python Package Index
* [Ansible role tester](https://github.com/ansible-community/molecule) `molecule` from Python Package Index
* [Kubernetes CLI client](https://www.docker.com/) `kubectl` from `docker.io/bitnami/kubectl`
* [OpenShift Origin CLI client](https://www.okd.io/) `oc`  from `docker.io/openshift/origin`
* [GitHub CLI](https://cli.github.com/) `gh` from official releases
* [Hashicorp Vault client](https://www.vaultproject.io/) `vault` from `docker.io/vault`
* [Trivy vulnerability scanner](https://github.com/aquasecurity/trivy) `trivy` from `docker.io/aquasec/trivy`
* [Shell checker](https://github.com/koalaman/shellcheck) `shellcheck` from `docker.io/koalaman/shellcheck:stable`
* [Dockerfile linter](https://github.com/hadolint/hadolint) `hadolint` from `docker.io/hadolint/hadolint`
* [YAML linter](https://yamllint.readthedocs.io/en/stable/) `yamllint` from Python Package Index
* [Jinja2 CLI](https://github.com/kolypto/j2cli) `j2` from Python Package Index
* [Certbot certificate automation tool](https://certbot.eff.org/) `certbot` from Python Package Index
* [YQ](https://github.com/mikefarah/yq) (YAML) command `yq` from `docker.io/mikefarah/yq`
* [JQ](https://stedolan.github.io/jq/) (JSON) command `jq` from Debian repository
* [Borg backup utility](https://www.borgbackup.org/) `borg` from official releases
* [Borgmatic backup utility](https://torsion.org/borgmatic/) `borgmatic` from Python Package Index
* [S3 command line client](https://s3tools.org/s3cmd) `s3cmd` from Python Package Index
* [ImapSync IMAP mailbox syncer](https://imapsync.lamiral.info/) `imapsync` from official releases
