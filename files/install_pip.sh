#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

pip3 'install' \
	--upgrade \
	--prefer-binary \
	"${@}"
