#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

# Set up the default source information
GIT_SOURCE='https://gitlab.com/mireiawenrose/docker-debian.git'
IMAGE_SOURCE='quay.io/mireiawen/debian:latest'
PERSISTENT='0'
PULL_IMAGE='1'

# Local binary installation location
BIN_DIR="${HOME}/.local/bin"

# Parse the command line
TEMP="$(getopt -n 'debian-installer' -o 'g:i:u:n:p:' --long 'git-source:,image-source:,username:,name:,persistent:,no-pull-image,no-binary-install' -- "${@}")"
eval set -- "${TEMP}"

while [ ${#} -gt 0 ]
do
	case "${1}" in
	'-g' | '--git-source')
		GIT_SOURCE="${2}"
		shift 2
		;;
	
	'-i' | '--image-source')
		IMAGE_SOURCE="${2}"
		shift 2
		;;
	
	'-u' | '--username')
		USERNAME="${2}"
		shift 2
		;;
	
	'-n' | '--name')
		NAME="${2}"
		shift 2
		;;
	
	'-p' | '--persistent')
		PERSISTENT_DIR="${2}"
		PERSISTENT='1'
		shift 2
		;;
	
	'--no-pull-image')
		PULL_IMAGE='0'
		shift
		;;

	'--no-binary-install')
		BIN_DIR='none'
		shift
		;;

	'--')
		shift
		break
		;;
	esac
done

# Set up the user information
if [ "${USERNAME:+x}" != 'x' ] || [ "${NAME:+x}" != 'x' ]
then
	echo 'Gathering user information...'
	if [ "${USERNAME:+x}" != 'x' ]
	then
		USERNAME="$(whoami)"
	fi
	
	if [ "${NAME:+x}" != 'x' ]
	then
		NAME="$(getent 'passwd' "${USERNAME}" |cut --delim ':' --field '5' |cut --delim ',' --field '1')"
	fi
fi

# Cleanup
function cleanup()
{
	echo 'Cleaning up...'
	rm --recursive --force "${TEMPDIR}"
}

# Run a command in a Docker container
function run_in_container()
{
	docker 'run' \
		--rm \
		--volume '/var/run/docker.sock:/var/run/docker.sock' \
		--volume "${BASE_DIR}:/work" \
		--workdir "${WORKDIR}" \
		--user "$(id -u "${USERNAME}"):$(id -g "${USERNAME}")" \
		--group-add "$(getent 'group' 'docker' | cut -d':' -f'3')" \
		"${IMAGE_SOURCE}" -c \
		"${@}"
}

# Create the binary file for the user
function create_local_bin()
{
	if [ "${BIN_DIR}" == 'none' ]
	then
		return
	fi
	
	echo 'Setting up the helper...'
	
	# Make sure the directory exists
	if [ ! -d "${BIN_DIR}" ]
	then
		mkdir --parents "${BIN_DIR}"
	fi
	
	if [ "${PERSISTENT}" == '1' ]
	then
		# Create symlink to the binary in the repo
		ln --force \
			"${BASE_DIR}/${REPO_DIR}/start-container" \
			"${BIN_DIR}/start-container"
	else
		# Copy the binary
		cp --force --remove-destination \
			"${BASE_DIR}/${REPO_DIR}/start-container" \
			"${BIN_DIR}/start-container"
	fi
	
}

# Pull the image from repo to use as base
if [ "${PULL_IMAGE}" == '1' ]
then
	echo 'Getting the base image...'
	docker 'pull' --quiet "${IMAGE_SOURCE}"
fi

# Create temp dir and trap the cleanup
echo 'Creating temporary folder...'
TEMPDIR="$(mktemp -d)"
trap cleanup EXIT

# Check if we want to set up persistency
if [ "${PERSISTENT}" == '1' ]
then
	BASE_DIR="$(realpath "$(dirname "${PERSISTENT_DIR}")")"
	REPO_DIR="$(basename "${PERSISTENT_DIR}")"
	PERSISTENT='1'
	echo "Using ${BASE_DIR}:${REPO_DIR} for Git repository"
else
	BASE_DIR="${TEMPDIR}"
	REPO_DIR='docker-debian'
fi

# Run the actual build
echo 'Fetching the source...'
WORKDIR='/work'
run_in_container \
	"[ -d '${REPO_DIR}' ] || git 'clone' --single-branch '${GIT_SOURCE}' '${REPO_DIR}'; \
	git -C '${REPO_DIR}' 'pull' --no-rebase"

echo 'Building the user image...'
WORKDIR="${WORKDIR}/${REPO_DIR}"
run_in_container \
	"bash 'build.sh' \
		--username '${USERNAME}' \
		--name '${NAME}' \
		--uid '${UID}'"

# Set up the helper script
create_local_bin
